# Attività 3 - Neo4j

### Descrizione

In questa repository sono presenti i file per la consegna dell'attività di Big Data su Neo4j. Ho scelto di studiare lo use proposto da neo4j sull'investigazione di frodi bancarie (https://neo4j.com/graphgists/bank-fraud-detection/). Partendo dalla struttura dello use case ho creato, attraverso l'uso del notebook presente nella directory "notebooks", un dataset di 100 utenti, ognuno con informazioni casuali e con relazioni casuali. Il dataset è suddiviso in file csv (contenuti nella cartella "data"), uno per ogni tipo di nodo e relazione. I file sono poi caricati all'interno del database tramite il "bulk loading" da riga di comando. Il totale dei nodi, relazioni e proprietà è rispettivamente 520, 420, 1522.
Il PDF "BDA - Attività Neo4j" contiene la descrizione dello use case, della struttura e dei dati del database e alcune query d'esempio.

### Comando per importare i dati

I dati sono presenti in formato CSV all'interno della cartella "data" e possono essere importati in un database attraverso il "bulk loading" da riga di comando. Se si dispone della versione enterprise si può creare da Neo4j Browser il database "bankfraud" con il comando `CREATE DATABASE bankfraud`. Il comando va dato dal database "system" ma funziona solo per la versione enterprise, perciò, in caso si abbia la versione community, l'import va eseguito sul database di default, chiamato Neo4j. Il comando va eseguito dalla cartella in cui sono presenti i dati, ossia bisogna spostarsi nella directory "data". Nel caso in cui si usi la versione enterprise si può cambiare "Neo4j" con "bankfraud" in `--database Neo4j`.

`
sudo neo4j-admin import --database Neo4j --nodes=AccountHolder=account_holders.csv --nodes=BankAccount=bank_accounts.csv --nodes=SSN=ssns.csv --nodes=Address=addresses.csv --nodes=CreditCard=credit_cards.csv --nodes=PhoneNumber=phone_numbers.csv --nodes=UnsecuredLoan=unsecured_loans.csv --relationships=HAS_BANKACCOUNT=has_bankaccount.csv --relationships=HAS_SSN=has_ssn.csv --relationships=HAS_ADDRESS=has_address.csv --relationships=HAS_CREDITCARD=has_credit_card.csv --relationships=HAS_PHONENUMBER=has_phone_number.csv --relationships=HAS_UNSECUREDLOAN=has_unsecured_loan.csv
`

### Query

Lo use case propone due query per l'analisi dei "fraud ring", alle quali si aggiungono quattro query ideate da me: una per arricchire la struttura del database con l'aggiunta di un arco con una proprietà e tre per l'analisi degli utenti, sfruttando anche la nuova relazione. Le query sono presenti nel file "query" per facilitarne il testing.